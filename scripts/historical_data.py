# -*- coding: utf-8 -*-
"""
Infer the sorting center of exit if necessary, and compute the indirect travel
distances according to the dataset for historical data.

Author: Thibaut Morel-Journel

Written for Python 3.7

"""

def check_results(df_expected, df_outcome):
    """
    Checks if the table of outcomes corresponds to the table of expected
    results. For the float results, the results are rounded to 1e-6, to prevent
    float errors.

    Parameters
    ----------
    df_expected: a table with the expected outcomes
    df_outcome: a table with the actual outcomes

    Returns
    -------
    Indicates if there are mismatches between the two tables, if not, prints
    'all outcomes correspond to expected'

    """
    print('Checking the test results:')
    no_error = True
    for col in df_expected.columns:
        is_float = df_expected.loc[:, col].dtype == 'float64'
        if is_float:
            l_errors = [i for i, row in df_outcome.iterrows() if
                        round(row.loc[col], 6) !=
                        round(df_expected.loc[i, col], 6)]
        else:
            l_errors = [i for i, row in df_outcome.iterrows() if
                        row.loc[col] != df_expected.loc[i, col]]
        for i in l_errors:
            print("    - error in row '" + str(i) + "'and column '" +  col
                  + "'")
            no_error = False
    if no_error:
        print('    - all outcomes correspond to expected')


def infer_exits(df_data, df_centre_location, d_movements):
    """
    Identify the exit centre of the batch so that the total travel distances of
    the bovines is minimal.

    Parameters
    ----------
    df_data: a table with the dataset considered
    df_centre_location: a table with the longitude and latitude of the sorting
    centres
    d_movements: a dictionnary with every movement considered

    Returns
    -------
    A dictionnary with the exit centre of each bovine.

    Relationship to other functions
    ---------------------------
    Calls the following function(s):
        -indirect_batch

    """
    d_exit_centre = {} # blank output

    for batch in set(df_data.batch):
        d_batch = {mov.index: mov for mov in d_movements.values() if
                   mov.batch == batch}
        d_centres = dict(df_data.loc[df_data.batch == batch, 'entry_centre'])
        # if all the bovines were sent to the same centre
        if len(set(d_centres.values())) == 1:
            d_exit_centre.update(d_centres)
            continue
        # sum of the travel distances going through each centre
        indirect = indirect_batch(df_centre_location, d_batch, d_centres)
        d_exit_centre.update({i: indirect.idxmin() for i in d_centres})

    return d_exit_centre


def indirect_batch(df_centre_location, d_batch, d_centres):
    """
    Compute the cumulated indirect distance of every bovine of a batch when
    going through each centre as their exit centre.

    Parameters
    ----------
    df_centre_location: a table with the longitude and latitude of the sorting
    d_batch: a dictionnary with every movement included in the batch considered
    d_centres: a dictionnary with the centres of entry of the movements included
    in the batch

    Returns
    -------
    A pandas series with the sum of the distances travelled by the bovines going
    through each centre as an exit centre.

    Relationship to other functions
    ---------------------------
    Called by the following function(s):
        -infer_exits

    """
    d_indirect = {centre:0 for centre in set(d_centres.values())}
    for centre in set(d_centres.values()):
        coord_exit = df_centre_location.loc[centre]
        for i, mov in d_batch.items():
            coord_entry = df_centre_location.loc[d_centres[i]]
            d_indirect[centre] += mov.indirect_distance(coord_entry, coord_exit)
    return pd.Series(d_indirect)


def compute_indirect(df_centre_location, d_movements, entires, exits):
    """
    Compute the indirect distances of each movement, based on the centre of
    entry and the centre of exit.

    Parameters
    ----------
    df_centre_location: a table with the longitude and latitude of the sorting
    d_movements: a dictionnary with every movement considered
    entires: the center of entry of each movement
    exits: the center of exit of each movement

    Returns
    -------
    A dictionnary with the indirect distances of each movement.

    """
    d_indirect = {}
    # identify every trajectory between entry centres and exit centres
    trajectories = pd.concat([entires, exits],
                             axis=1).drop_duplicates().dropna()

    for en, ex in trajectories.values:
        selected = entires.index[(entires == en) & (exits == ex)]
        coord_en = df_centre_location.loc[en]
        coord_ex = df_centre_location.loc[ex]
        d_selected = {i: d_movements[i].indirect_distance(coord_en,
                                                          coord_ex) for
                      i in selected}
        d_indirect.update(d_selected)
    return d_indirect


if __name__ == '__main__':

    # import modules
    import argparse
    import pandas as pd # version 0.23.4
    import os
    import sys

    # add the path to the current directory
    sys.path.append(os.path.abspath(os.path.curdir))
    import src # the code for the algorithm


    # argument parser for the script
    parser = argparse.ArgumentParser(description="""Script to generate the
                                     original dispersal distances""")
    parser.add_argument('folder', type=str,
                        help="""the name of the folder containing the data""")
    FOLDER = vars(parser.parse_args())['folder']
    print("Defining the sorting centers and the indirect distances for \nthe " +
          "historical data: '" + FOLDER + "':")

    is_test = FOLDER == 'test'

    if is_test:
        data_path = './test/data/'
    else:
        data_path = './data/' + FOLDER # path to the data files

    # load the dataset
    print("  Loading the data")
    try:
        df_data = pd.read_csv(data_path + '/dataset.csv', index_col=0,
                              parse_dates=['arrival_date', 'exit_date'])
    except FileNotFoundError:
        raise ValueError('The dataset of bovines is missing')

    # load the outcomes
    if is_test:
        df_output = df_data.loc[:, ['batch', 'entry_centre']].dropna()
        output_exists = False
    else:
        try:
            df_output = pd.read_csv('./outcomes/' + FOLDER + '/output_raw.csv',
                                    index_col=0)
        except FileNotFoundError:
            df_output = df_data.loc[:, ['batch', 'entry_centre']].dropna()
            output_exists = False
            print("  Info: there are no optimized distances for this folder")
        else:
            df_output.at[:, 'entry_centre'] = df_data.loc[df_output.index,
                                                          'entry_centre']
            output_exists = True
            print("  Info: there are optimized distances for this folder")

    # load the holding location list
    try:

        df_holding_location = pd.read_csv(data_path + '/holding_location.csv',
                                          index_col=0)
    except FileNotFoundError:
        raise ValueError('The coordinate list of the holdings is missing')

    # load the centre location list
    try:
        df_centre_location = pd.read_csv(data_path + '/centre_location.csv',
                                         index_col=0)
    except FileNotFoundError:
        raise ValueError('The coordinate list of the centres is missing')

    # check if the entry centre of the batches is indicated
    try:
        df_data.entry_centre
    except:
        raise ValueError('The original sorting centres are missing')

    # create a dictionnary of movements
    print("  Creating the list of movements")
    d_movements = {i: src.Movement(row, df_holding_location) for
                   i, row in df_data.loc[df_output.index].iterrows()}

    # check if the exit centre of the batches is indicated
    try:
        df_data.exit_centre
    except:
        print("  Infering the exit centres of the batches")
        d_exit_centres = infer_exits(df_data.loc[df_output.index],
                                     df_centre_location,
                                     d_movements)
        df_output.at[:, 'exit_centre'] = pd.Series(d_exit_centres)


    # compute the indirect distance
    print("  Computing the original indirect distances")
    d_indirect = compute_indirect(df_centre_location, d_movements,
                                  df_output.entry_centre, df_output.exit_centre)
    df_output.at[:, 'indirect_original'] = pd.Series(d_indirect)

    # save the outcomes
    os.makedirs('./outcomes/' + FOLDER, exist_ok=True)
    if output_exists:
        print("  Saving the output and the original itinerary and distances")
        df_output.to_csv('./outcomes/' + FOLDER + '/output_hist.csv')
    else:
        print("  Saving original itinerary and distances")
        df_output.to_csv('./outcomes/' + FOLDER + '/hist_only.csv')

    # run a check of the results if the folder is 'test'
    if is_test:
        df_expected = pd.read_csv('./test/outcomes/expected_output_hist.csv',
                                  index_col=0)
        check_results(df_expected, df_output)

############################### END OF THE SCRIPT ##############################
