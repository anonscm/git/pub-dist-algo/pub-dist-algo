# Produces standard graphical outputs for the distance optimization. Three .png
# files can be created by 'png_distribution_indirect', png_distribution_ratio'
# and png_per centre'. Besides the default graphical options, the script can
# also accept other graphical parameters if they are in a file named
# 'graphical_options.csv, available with the dataset.
#
# Author: Thibaut Morel-Journel
#
# Written for R.6.0
#


png_distribution <- function(l_values, l_colors, path, name, png_width=12,
                             png_height=12, resolution=300, class_size=NULL,
                             threshold=NULL, xlab="", is_log=FALSE, xmin=NULL,
                             xmax=NULL, ymin=NULL, ymax=NULL){
    #' PNG of distributions
    #'
    #' @description Creates a png file with points and polygons representing
    #' agregated distibutions.
    #'
    #' @usage png_distribution_indirect(l_values, l_colors, path, name, png_width=12,
    #' png_height=12, resolution=300, class_size=NULL, threshold = NULL, xlab="",
    #' is_log=FALSE, xmin=NULL, xmax=NULL, ymin=NA, ymax=NA)
    #'
    #' @param l_values A list of values per treatment.
    #' @param l_colors A list of each color to use for each distribution.
    #' @param path The path to the folder in which the file will be created.
    #' @param name The name of the file created.
    #' @param png_width Optional. The width of the picture (in cm). Default to
    #' 12.
    #' @param png_height Optional. The height of the picture (in cm). Default to
    #' 12.
    #' @param resolution Optional. The resolution of the picture (in ppi).
    #' Default to 300.
    #' @param class_size Optional. The size of each class when the data is
    #' discretized. Computed automatically otherwise.
    #' @param threshold Optional. The value at which a threshold must be
    #' displayed on the graphic.
    #' @param xlab Optional. The name of the X-axis. Default to an empty string.
    #' @param is_log Optional. A boolean indicating whether the X-axis should be
    #' on a log-scale. Default to FALSE.
    #' @param xmin Optional. The minimal value of the X-axis. Computed
    #' automatically otherwise.
    #' @param xmax Optional. The maximal value of the X-asis. Computed
    #' automatically otherwise.
    #' @param ymin Optional. The minimal value of the Y-axis. Computed
    #' automatically otherwise.
    #' @param ymax Optional. The maximal value of the Y-axis. Computed
    #' automatically otherwise.
    #'
    #' @return The function returns nothing, but creates a PNG file in the
    #' folder indicated.
    #'
    #' @details The function passes arguments to the functions
    #' "plot_distribution", "compute_xlim" and "compute_class_size", which are
    #' called by this function.

    # compute the limits of the X-axis
    xlim <- compute_xlim(l_values, xmin, xmax)
    # compute the class size
    if(is.null(class_size)){
        class_size <- compute_class_size(l_values, xlim)
    }
    png(
        filename=paste0(path, name),
        width=png_width,
        height=png_height,
        units="cm",
        res=resolution
    )
    par(mar=c(4,4,1,1))
    plot_distribution(l_values, l_colors, class_size, xlab, is_log, xlim,
                      ymin, ymax)
    if(!is.null(threshold)){
        abline(v=threshold, lty=2)
    }
    not_display <- dev.off() # prevent displaying the message
}


png_per_centre <- function(l_values, centres, l_colors, path, name,
                           png_width=12, png_height=NULL, resolution=300,
                           threshold=NULL, xlab_raw="", xlab_prop="",
                           is_raw_log=FALSE, xmin_raw=NULL, xmax_raw=NULL,
                           xmin_prop=0, xmax_prop=NULL){
    #' PNG of values per centre
    #'
    #' @description Creates a png file with raw data per centre on the left, and
    #'  the proportion of data per centre on the right.
    #'
    #' @usage png_per_centre(l_values, centres, l_colors, path, name, png_width=12,
    #' png_height=NULL, resolution=300, threshold=NULL, xlab_raw="", xlab_prop="",
    #' is_raw_log=FALSE, xmin_raw=NULL, xmax_raw=NULL, xmin_prop=0, xmax_prop=NULL)
    #'
    #' @param l_values A list of values per treatment.
    #' @param centres An ordered vector of the centres to consider
    #' @param l_colors A list of each color to use for each distribution.
    #' @param path The path to the folder in which the file will be created.
    #' @param name The name of the file created.
    #' @param png_width Optional. The width of the picture (in cm). Default to
    #' 12.
    #' @param png_height Optional. The height of the picture (in cm). Computed
    #' automatically otherwise.
    #' @param resolution Optional. The resolution of the picture (in ppi).
    #' Default to 300.
    #' @param threshold Optional. The value at which a threshold must be
    #' displayed on the graphic.
    #' @param xlab_raw Optional. The name of the X-axis for the raw data.
    #' Default to an empty string.
    #' @param xlab_prop Optional. The name of the X-axis for the proportions.
    #' Default to an empty string.
    #' @param is_raw_log Optional. A boolean indicating whether the X-axis of
    #' the raw data should be on a log-scale. Default to FALSE.
    #' @param xmin_raw Optional. The minimal value of the X-axis for the raw
    #' data. Computed automatically otherwise.
    #' @param xmax_raw Optional. The maximal value of the X-asis for the raw
    #' data. Computed automatically otherwise.
    #' @param xmin_prop Optional. The minimal value of the X-axis for the
    #' proportions. Default to 0.
    #' @param xmax_prop Optional. The maximal value of the X-asis for the
    #' porportions. Computed automatically otherwise.
    #'
    #' @return The function returns nothing, but creates a PNG file in the
    #' folder indicated.
    #'
    #' @details The function passes arguments to the functions
    #' "plot_raw", "plot_proportion" and compute_xlim", which are called by this
    #' function.


    # compute the proportion by centre
    l_proportions <- lapply(
        X=l_values,
        FUN=function(values){
            sapply(values, length)/length(unlist(values))
        }
    )
    # compute the limits of the X-axis for the raw data and the proportions
    xlim_raw <- compute_xlim(l_values, xmin_raw, xmax_raw)
    xlim_prop <- compute_xlim(l_proportions, xmin_prop, xmax_prop)
    png(
        filename=paste0(path, name),
        width=png_width,
        height=png_width/2*length(l_values),
        units="cm",
        res=resolution
    )
    add_space <- max(c(grep("\\n", xlab_raw), grep("\\n", xlab_prop)))
    par(mfrow=c(length(l_values), 2), oma=c(4 + add_space, 0, 0, 0))
    for(i in length(l_values):1){
        par(mar=c(1, 1, 1, 1))
        plot_raw(
            l_arrays=l_values[[i]],
            color=l_colors[[i]],
            is_log=is_raw_log,
            xlim=xlim_raw,
            threshold=threshold
            )
        if(i==1){
           mtext(xlab_raw, 1, 3 + sum(grep("\n", xlab_raw)))
        }
        plot_proportion(
            l_arrays=l_values[[i]],
            names=centres,
            color=l_colors[[i]],
            xlim=xlim_prop,
            threshold=threshold
        )
        if(i==1){
            mtext(xlab_prop, 1, 3 + sum(grep("\n", xlab_prop)))
        }
    }
    not_display <- dev.off()
}


plot_distribution <- function(l_values, l_colors, class_size, xlab, is_log, xlim,
                              ymin, ymax){
    #' Plot of distributions
    #'
    #' @description Plot points and polygons representing agregated
    #' distibutions.
    #' @usage plot_distribution(l_values, l_colors, class_size, xlab, xlim, ylim)
    #'
    #' @param l_values A list of indirect distances per treatment.
    #' @param l_colors A list of each color to use for each distribution.
    #' @param class_size The size of each class when the data is discretized.
    #' @param xlab The name of the X-axis.
    #' @param is_log Optional. A boolean indicating whether the X-axis should be
    #' @param xlim The minimal and maximal values of the X-axis
    #' @param ylim The minimal and maximal values of the Y-axis
    #'
    #' @return The function returns nothing, but plots the distributions.

    # compute the X-values
    x_vals <- class_size*((xlim[1]/class_size):(xlim[2]/class_size))
    # compute the Y-values for the points
    l_points <- lapply(
        X=l_values,
        FUN=function(values){
            values_distribution(unlist(values), class_size, xlim)
        }
    )
    # compute the Y-values for the lines
    l_lines <- lapply(
        X=l_values,
        FUN=function(values){
            smoothed_distribution(unlist(values), class_size, xlim, 5)
        }
    )
    # compute the limits of the Y-axis
    ylim <- compute_ylim(l_lines, ymin, ymax)

    # create the plot
    plot(
        NULL,
        xlab=xlab,
        ylab="Frequency",
        xlim=xlim,
        ylim=ylim,
        log=ifelse(is_log, "x", ""),
        bty="n"
    )
    for(i in 1:length(l_values)){
        points(
            x=x_vals,
            y=l_points[[i]],
            pch=16,
            col=l_colors[[i]],
            cex=.5)
        lines(
            x=x_vals,
            y=l_lines[[i]],
            col=l_colors[[i]]
        )
        polygon(
            x=c(x_vals, rev(x_vals)),
            y=c(l_lines[[i]], rep(0, length(x_vals))),
            col=paste0(l_colors[[i]], "30"),
            border=NA
        )
    }
}


plot_raw <- function(l_arrays, color, is_log, xlim, threshold){
    #' Plot of raw data
    #'
    #' @description Plot points representing raw data per array.
    #' @usage plot_raw(l_arrays, color, xlim, is_log, threshold)
    #'
    #' @param l_arrays A list of the arrays of values to represent.
    #' @param color The color of the points.
    #' @param is_log Optional. A boolean indicating whether the X-axis should be
    #' @param xlim The minimal and maximal values of the X-axis
    #' @param threshold The value at which a threshold must be displayed on the
    #' graphic, if any.
    #'
    #' @return The function returns nothing, but plots the raw data.

    plot(
        NULL,
        xlab="",
        xlim=rev(xlim),
        ylim=c(length(l_arrays) + .5, .5),
        log=ifelse(is_log, "x", ""),
        yaxt='n',
        bty="n"
    )
    for(i in 1:length(l_arrays)){
        values <- l_arrays[[i]]
        #compute the transparency value
        transp_dec = round((2e3/length(l_arrays))/(log(length(values)) + 1))
        transp_hex <- format(as.hexmode(transp_dec), width=2)
        # plot the raw data
        points(
            x=values,
            y=rnorm(length(values), i, .1),
            pch=16,
            col=paste0(color, transp_hex),
            cex=.3
        )
    }
    if(!is.null(threshold)){
        abline(v=threshold, lty=2, lw=2)
    }
}


plot_proportion <- function(l_arrays, names, color, xlim, threshold){
    #' Plot of proportion per centre
    #'
    #' @description Plot barplots representing the proportion of values per
    #' array.
    #' @usage plot_raw(l_arrays, color, xlim, is_log, threshold)
    #'
    #' @param l_arrays A list of the arrays of values to represent.
    #' @param names The names of the arrays to consider
    #' @param color The color of the points.
    #' @param xlim The minimal and maximal values of the X-axis
    #' @param threshold The value at which a threshold must be displayed on the
    #' graphic, if any.
    #'
    #' @return The function returns nothing, but plots the proportions.

    # compute the limits of the Y-axis
    ylim = c(length(l_arrays)*1.25, 0)
    thresholds = c(-Inf, threshold)
    # empty plot
    plot(
        NULL,
        xlab="",
        ylab="",
        xlim=xlim,
        ylim=ylim,
        yaxt='n',
        bty="n"
    )
    for(th in thresholds){
        # compute the propotions per array
        proportions <- sapply(
            X=l_arrays,
            FUN=function(array) sum(array > th)
        )/length(unlist(l_arrays))
        # create the barplot
        barplot(
            height=proportions,
            names.arg=names,
            horiz=TRUE,
            col=paste0(color, "60"),
            las=1,
            add=T
        )
    }
}


values_distribution <- function(values, class_size, xlim){
    #' Distribution of the values
    #'
    #' @description Computes the frequencies of each class defined
    #' @usage values_distribution(values, class_size, xlim)
    #'
    #' @param values A list of indirect distances per treatment.
    #' @param class_size The size of each class when the data is discretized.
    #' @param xlim The minimal and maximal values of the X-axis
    #'
    #' @return The frequency of values for each class.


    limits <- class_size*((xlim[1]/class_size):(xlim[2]/class_size + 1))
    occurences <- sapply(
        X=2:length(limits),
        FUN=function(i){
            sum((values >= limits[i - 1]) & (values < limits[i]))
        }
    )
    return(occurences/length(values))
}


smoothed_distribution <- function(values, class_size, xlim, smoothing){
    #' Smoothed distribution
    #'
    #' @description Computes the values to get a smoothed distribution by
    #' running median.
    #' @usage smoothed_distribution(values, class_size, xlim, smoothing)
    #'
    #' @param values A list of indirect distances per treatment.
    #' @param class_size The size of each class when the data is discretized.
    #' @param xlim The minimal and maximal values of the X-axis
    #' @param smoothing The window to consider for the running median
    #'
    #' @return The frequency of values for each class.


    fine_grain <- values_distribution(values, class_size/smoothing, xlim)
    smoothed <- runmed(
        x=fine_grain,
        k=smoothing*(smoothing + ifelse(smoothing%%2==1, 0, 1))
    )
    return(
        sapply(
            X=1:ceiling(length(fine_grain)/smoothing),
            FUN=function(i){
                sum(smoothed[((i - 1)*smoothing) + (1:smoothing)], na.rm=T)
            }
        )
    )
}


compute_class_size <- function(x, xlim){
    #' Size of the classes for discretization
    #'
    #' @description Computes the size of the classes when discretizing a set of
    #' values on the X-axis.
    #'
    #' @usage compute_class_size(x)
    #'
    #' @param x A vector or list with the values of the X-axis to consider.
    #' @param xlim A vector with the limits of the X-axis to consider
    #'
    #' @return The size of the classes.
    all_values <- unlist(x)
    all_values <- all_values[(all_values >= xlim[1]) & (all_values <= xlim[2])]
    all_quantiles <- quantile(all_values, c(1e-3, 1 - 1e-3), na.rm=TRUE)
    diff_quantiles <- diff(all_quantiles)
    return(diff_quantiles^(1.4)*.001)
}


compute_xlim <- function(x, xmin, xmax){
    #' Limits of the X-axis
    #'
    #' @description Computes the limits of the X-axis so that most of the data
    #' is between the limits.
    #'
    #' @usage compute_xlim(x, xmin, xmax)
    #'
    #' @param x A vector or list with the values of the X-axis to consider.
    #' @param xmin The value of min for the X-axis provided beforehand.
    #' @param xmax The value of max for the X-axis provided beforehand.
    #'
    #' @return A vector with the limits of the X-axis

    if(!is.null(xmin) & !is.null(xmax)){
        return(c(xmin, xmax))
    }
    all_values <- unlist(x)
    all_values <- all_values[!is.infinite(all_values)]
    all_quantiles <- quantile(all_values, c(0, 1e-3, 1 - 1e-3, 1), na.rm=TRUE)
    diff_quantiles <- diff(all_quantiles)[2]
    xmin <- ifelse(
        test=!is.null(xmin),
        yes=xmin,
        no=ifelse(
            test=diff(all_quantiles)[1] < diff_quantiles*.1,
            yes=all_quantiles[1],
            no=all_quantiles[2] - diff_quantiles*.1
        )
    )
    xmax <- ifelse(
        test=!is.null(xmax),
        yes=xmax,
        no=ifelse(
            test=diff(all_quantiles)[3] < diff_quantiles*.1,
            yes=all_quantiles[4],
            no=all_quantiles[3] + diff_quantiles*.1
        )
    )
    return(c(xmin, xmax))
}


compute_ylim <- function(x, ymin, ymax){
    #' Limits of the Y-axis
    #'
    #' @description Computes the limits of the Y-axis so that most of the data
    #' is between the limits. The values must be between 0 and 1.
    #'
    #' @usage compute_xlim(x, ymin, ymax)
    #'
    #' @param x A vector or list with the values of the Y-axis to consider.
    #' @param ymin The value of min for the Y-axis provided beforehand.
    #' @param ymax The value of max for the Y-axis provided beforehand.
    #'
    #' @return A vector with the limits of the Y-axis
    #'
    if(!is.null(ymin) & !is.null(ymax)){
        return(c(ymin, ymax))
    }
    all_values <- unlist(x)
    all_values <- all_values[!is.infinite(all_values)]
    all_quantiles <- quantile(all_values, c(0, 1))
    ymin <- ifelse(
        test=!is.null(ymin),
        yes=ymin,
        no=ifelse(
            test=all_quantiles[1] - diff(all_quantiles)*.1 < 0,
            yes=0,
            no=all_quantiles[1] - diff(all_quantiles)*.1
        )
    )
    ymax <- ifelse(
        test=!is.null(ymax),
        yes=ymax,
        no=ifelse(
            test=all_quantiles[2] + diff(all_quantiles)*.1 > 1,
            yes=1,
            no=all_quantiles[2] + diff(all_quantiles)*.1
        )
    )
    return(c(ymin, ymax))
}



# check the arguments
args = commandArgs(trailingOnly=TRUE)
if(length(args) != 1){
    stop("The name of the folder must be provided")
}
FOLDER = args[1]

# path to the outcomes to load
path_data <- ifelse(
    test=FOLDER == 'test',
    yes="./test/outcomes/",
    no=paste0("./outcomes/", FOLDER, "/")
)
# path to the folder where the graphics are saved
path_graphics <- ifelse(
    test=FOLDER == 'test',
    yes="./outcomes/test/graphics/",
    no=paste0(path_data, "graphics/")
)

file_list <- list.files(path_data, ".csv")

# get the variables used to create the graphics
message(paste0("Create default graphics for the folder '", FOLDER, "':"))
if(FOLDER == 'test'){
    l_variables <- c(
        as.list(read.csv(
            paste0(path_data, "expected_output_raw.csv")
        )[, c(4, 5, 3)]),
        as.list(read.csv(
            paste0(path_data, "expected_hist_only.csv")
        )[, c(5, 3)])
    )
} else if(length(grep("output", file_list)) > 0){
    l_variables <- as.list(read.csv(
            paste0(path_data, grep("output", file_list,value=TRUE)[1])
        )[, c(4, 5, 3)])
    if("hist_only.csv" %in% file_list){
        l_variables <- append(
            l_variables,
            as.list(read.csv(paste0(path_data, "hist_only.csv"))[, c(5, 3)])
        )
    }
    if("output_hist.csv" %in% file_list){
        l_variables <- append(
            l_variables,
            as.list(read.csv(paste0(path_data, "output_hist.csv"))[, c(8, 6)])
        )
    }
} else {
    stop("No outcome file in the folder")
}

# create the lists of data used to create the graphics
centres <- sort(unique(l_variables[[3]]))
l_data <- lapply(
    X=list(l_variables[[2]], l_variables[[2]]/l_variables[[1]]),
    FUN=function(x){
        list(lapply(
            X=sort(unique(l_variables[[3]])),
            FUN=function(centre){
                x[l_variables[[3]] == centre]
            }
        ))
    }
)
if(length(l_variables) == 5){
    for(i in 1:2){
        x <- list(l_variables[[4]], l_variables[[4]]/l_variables[[1]])[[i]]
        l_data[[i]] <- append(
            l_data[[i]],
            list(lapply(
                X=sort(unique(l_variables[[5]])),
                FUN=function(centre){
                    x[l_variables[[5]] == centre]
                }
            ))
        )
    }
}

# set the argument lists
l_args <- list(
    'png_distribution'=list(
        'l_values'=l_data[[1]],
        'l_colors'=list("#FF7340", "#0F2080"),
        'path'=path_graphics,
        'name'="distribution_indirect.png",
        'xlab' ="Indirect distance (km)"
    ),
    'png_distribution'=list(
        'l_values'=l_data[[2]],
        'l_colors'=list("#FF7340", "#0F2080"),
        'path'=path_graphics,
        'name'="distribution_ratio.png",
        'png_height'=6,
        'xlab' ="Ratio of indirect to direct distances",
        'is_log' = TRUE
    ),
    'png_per_centre'=list(
        'l_values'=l_data[[2]],
        'l_colors'=list("#FF7340", "#0F2080"),
        'path'=path_graphics,
        'centres'=centres,
        'name'="per_centre.png",
        'xlab_raw' ="Ratio of indirect \nto direct distances",
        'xlab_prop' ="Proportion of transfers \nper centre",
        'is_raw_log'=TRUE
    )
)

# check if there are graphical parameters to handle
if(file.exists(paste0('./data/', FOLDER, '/graphical_options.csv'))){
    df_optional <- read.csv(paste0('./data/', FOLDER, '/graphical_options.csv'),
                            as.is=TRUE)
    for(i in as.numeric(rownames(df_optional))){
        l_to_append <- list(df_optional$val[i])
        names(l_to_append) <- df_optional$arg[i]
        k = which(sapply(l_args, function(x) x$name == df_optional$file[i]))
        # remove the original argument if already existing
        l_args[[k]][which(names(l_args[[k]]) == names(l_to_append))] <- NULL
        # add the new argument
        l_args[[k]] <- append(l_args[[k]], l_to_append)
    }
}

# create the graphics
if(!dir.exists(path_graphics)) dir.create(path_graphics)
for(i in 1:length(l_args)){
    message(paste0("  Plotting '", l_args[[i]]$name, "'"))
    do.call(what=names(l_args)[[i]], args=l_args[[i]])
}
