# -*- coding: utf-8 -*-
"""
Runs the the algorithm of distacnce optimization.

Author: Thibaut Morel-Journel

Written for Python 3.7

"""


# import modules
import argparse
import os
import sys
import pandas as pd # version 0.23.4

# add the path to the current directory
sys.path.append(os.path.abspath(os.path.curdir))
import src # the code for the algorithm


def check_results(df_expected, df_outcome):
    """
    Checks if the table of outcomes corresponds to the table of expected
    results. For the float results, the results are rounded to 1e-6, to prevent
    float errors.

    Parameters
    ----------
    df_expected: a table with the expected outcomes
    df_outcome: a table with the actual outcomes

    Returns
    -------
    Indicates if there are mismatches between the two tables, if not, prints
    'all outcomes correspond to expected'

    """
    print('Checking the test results:')
    no_error = True
    for col in df_expected.columns:
        is_float = df_expected.loc[:, col].dtype == 'float64'
        if is_float:
            l_errors = [i for i, row in df_outcome.iterrows() if
                        round(row.loc[col], 3) !=
                        round(df_expected.loc[i, col], 3)]
        else:
            l_errors = [i for i, row in df_outcome.iterrows() if
                        row.loc[col] != df_expected.loc[i, col]]
        for i in l_errors:
            print("    - error in row '" + str(i) + "'and column '" +  col
                  + "'")
            no_error = False
    if no_error:
        print('    - all outcomes correspond to expected')


# argument parser for the script
parser = argparse.ArgumentParser(description="""Runs the algorithm of distance
                                 optimization""")
parser.add_argument('folder', type=str,
                    help="""the name of the folder containing the data""")
FOLDER = vars(parser.parse_args())['folder']
print("\nAlgorithm of distance optimization for the dataset '" + FOLDER + "':")
is_test = FOLDER == 'test'

# load the dataset
print("  Loading the data")
if is_test:
    data_path = './test/data'
else:
    data_path = './data/' + FOLDER # path to the data files

try:
    df_data = pd.read_csv(data_path + '/dataset.csv', index_col=0,
                          parse_dates=['arrival_date', 'exit_date'])
except FileNotFoundError:
    raise ValueError('The dataset of bovines is missing')

# check and remove rows with missing values
l_columns = ['batch', 'origin', 'INSEE_origin', 'destination',
             'INSEE_destination', 'arrival_date', 'exit_date']
d_missing_values = {name: col.index[col.isna()] for
                    name, col in df_data.loc[:, l_columns].iteritems()}
for col, indices in d_missing_values.items():
    for index in indices:
        df_data = df_data.drop(index)
        print("    - row '" + str(index) + "' removed because of missing " +
              "value for '" + col + "'")
    if col in l_columns[:5]:
        df_data.loc[:, col] = df_data.loc[:, col].astype('int64')
    if col in l_columns[5:]:
        df_data.loc[:, col] = df_data.loc[:, col].astype('datetime64')

# remove the direct transfers
try:
    df_data.entry_centre
except FileNotFoundError:
    None
else:
    print("  Removing the direct transfers from the dataset")
    df_data = df_data.dropna(subset=['entry_centre'])

# load the holding location list
try:
    df_holding_location = pd.read_csv(data_path + '/holding_location.csv',
                                      index_col=0)
except FileNotFoundError:
    raise ValueError('The coordinate list of the holdings is missing')

# load the centre location list
try:
    df_centre_location = pd.read_csv(data_path + '/centre_location.csv',
                                     index_col=0)
except FileNotFoundError:
    raise ValueError('The coordinate list of the centres is missing')

# load the maximal capacities of the centres
try:
    df_capacity = pd.read_csv(data_path + '/centre_capacity.csv', index_col=0)
except FileNotFoundError:
    raise ValueError('The list of maximal capacities is missing')


# create the output table
df_output = df_data.loc[:, ['batch']]

# create a dict of movements and compute the coordinates of the holdings
d_movements = {i: src.Movement(row, df_holding_location) for
               i, row in df_data.iterrows()}

# run the algorithm
print("  Selecting the optimal centres")
d_centre = src.run_distance_optimization(df_capacity, df_centre_location,
                                         d_movements)
df_output.at[:, 'centre'] = [d_centre[batch] for batch in df_data.batch]

# compute the direct distances
print("  Computing the direct distances")
for i, mov in d_movements.items():
    df_output.at[i, 'direct_distance'] = mov.direct_distance

# compute the indirect distances
print("  Computing the indirect distances")
for i, mov in d_movements.items():
    centre_coord = df_centre_location.loc[df_output.loc[i, 'centre']]
    df_output.at[i, 'indirect_distance'] = mov.indirect_distance(centre_coord)


# save the outcomes
os.makedirs('./outcomes/' + FOLDER, exist_ok=True)
df_output.to_csv('./outcomes/' + FOLDER + '/output_raw.csv')
print("  Saving the output")

# run a check of the results if the folder is 'test'
if is_test:
    df_expected = pd.read_csv('./test/outcomes/expected_output_raw.csv',
                              index_col=0)
    check_results(df_expected, df_output)

################################ END OF THE CODE ###############################
