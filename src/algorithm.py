# -*- coding: utf-8 -*-
"""
This module regroups the classes and functions used to run the algorithm of
distance optimization.

It is structured as follows:

The class 'Movement' regroups informations about the movements of bovines from
their holding of origin to their holding of destination. A movement is defined
by its name, batch, date of arrival to the centre, date of exit from the centre,
holding of origin and holding of destination.

The functions used to perform the distance optimization:

    -run_distance_optimization: the main function calling the other functions
    and formatting the output. The function returns a dictionnary whose keys are
    each batch and whose values are the centre they were assigned.
    This main function works sequentially by iterating over the days at which
    the bovines are managed. Each day, each enterling batch is assigned to a
    centre, and each exiting batch is removed from its centre. The centres
    assigned are recorded to create the ouptut.

    -date_per_batch: creates a dictionnary whose keys are each batch and whose
    values are their date of entry and exit form centres.

    -manage_exits: removes bovines exiting the centres from the current
    occupancy of the centres.

    -compute_indirect: computes the total travel distances of each batch when
    going through each centre

    -select_batch: select the next batch to manage based on the difference
    between the total travel dinstance of the batch going thourhg the best
    centre and the second best centre.

    -compute_distance: computes the direct distances (i.e. as the crow flies)
    between two sets of coordinates.

"""

import numpy as np # version 1.15.1
import pandas as pd # version 0.23.4


class Movement:
    """
    The movement of a bovine from their holding of origin to their holding of
    destination.

    Parameters
    ----------
    row: a row from the dataset of bovines
    df_holding_location: a table of coordinates of the holdings based on their
    INSEE code

    """
    def __init__(self, row, df_holding_location):
        self.index = row.name
        self.batch = row.batch
        self.arrival_date = row.arrival_date
        self.exit_date = row.exit_date
        self.origin = df_holding_location.loc[row.INSEE_origin]
        self.destination = df_holding_location.loc[row.INSEE_destination]

    @property
    def direct_distance(self):
        """
        Computes the direct distance from the holding of origin to the holding
        of destination.

        Parameters
        ----------
        None

        Returns
        -------
        The direct distance

        Relationship to other functions
        -------------------------------
        Calls the following function(s):
            -compute_distance

        """
        return compute_distance(self.origin, self.destination)

    def indirect_distance(self, centre_entry, centre_exit=None):
        """
        Computes the indirect distance from the holding of origin to the holding
        of destination going through one or two centres.

        Parameters
        ----------
        centre_entry: the coordinates of the centre of arrival of the bovine
        from the holding of origin
        centre_exit: the coordinates of the centre of arrival of the bovine from
        the holding of destination

        Returns
        -------
        The indirect distance

        Relationship to other functions
        -------------------------------
        Calls the following function(s):
            -origin_to_centre
            -centre_to_destination
            -compute_distance

        """
        if centre_exit is None:
            centre_exit = centre_entry

        from_origin = self.origin_to_centre(centre_entry)
        to_destination = self.centre_to_destination(centre_exit)
        between_centres = compute_distance(centre_entry, centre_exit)
        return sum([from_origin, to_destination, between_centres])

    def origin_to_centre(self, centre):
        """
        Computes the distance from the holding of origin to the centre.

        Parameters
        ----------
        centre: the coordinates of the centre

        Returns
        -------
        The distance from the origin to the centre

        Relationship to other functions
        -------------------------------
        Called by the following function(s):
            -indirect_distance
        Calls the following function(s):
            -compute_distance

        """
        return compute_distance(self.origin, centre)

    def centre_to_destination(self, centre):
        """
        Computes the distance from the centre to the holding of destination.

        Parameters
        ----------
        centre: the coordinates of the centre

        Returns
        -------
        The distance from the centre to the destination

        Relationship to other functions
        -------------------------------
        Called by the following function(s):
            -indirect_distance
        Calls the following function(s):
            -compute_distance

        """
        return compute_distance(centre, self.destination)


def run_distance_optimization(df_capacity, df_centre_location, d_movements,
                              occupancy=None):
    """
    Runs the algorithm of distance optimization.

    Parameters
    ----------
    df_capacity: a table with the maximal capacitiy of each centre for each year
    df_centre_location: a table with the coordinates of each centre
    d_movements: a dictionnary with every movement considered
    occupancy: a pandas series with the initial occupancy of each centre

    Returns
    -------
    A dictionnary with the centres assigned to each batch

    Relationship to other functions
    -------------------------------
    Calls the following function(s):
        -date_per_batch
        -manage_exits
        -compute_indirect
        -select_batch
        -compute_distance

    """
    d_ouptut = {} # blank output

    # set the initial occupancies of the centres
    if occupancy is None:
        occupancy = pd.Series(0, index=df_capacity.index)

    # compute the arrival and exit dates of the batches
    d_arrivals = date_per_batch(d_movements, 'arrival_date')
    d_exits = date_per_batch(d_movements, 'exit_date')

    for day in sorted(set(d_arrivals.values()).union(d_exits.values())):
        # manage the entering movements
        l_entering = [batch for batch in d_arrivals if d_arrivals[batch] == day]
        df_indirect = compute_indirect(df_centre_location, d_movements,
                                       l_entering)
        while l_entering:
            # check for available centres
            is_available = occupancy < df_capacity.loc[:, str(day.year)]
            # select the batch to assign
            selected = select_batch(df_indirect, l_entering, is_available)
            # assign the centre
            centre = df_indirect.loc[selected][is_available].idxmin()
            d_ouptut[selected] = centre
            # change the occupancy
            occupancy[centre] += sum([True for mov in d_movements.values() if
                                      mov.batch == selected])
            # remove the movement from the list of entering movements
            idx_selected = [i for i, val in enumerate(l_entering) if
                            val == selected][0]
            l_entering.pop(idx_selected)
        # manage the exiting movements
        l_exiting = [batch for batch in d_exits if d_exits[batch] == day]
        manage_exits(d_movements, d_ouptut, l_exiting, occupancy)

    return d_ouptut


def date_per_batch(d_movements, attr):
    """
    Creates a dictionnary of dates for each batch.

    Parameters
    ----------
    d_movements: a dictionnary with every movement considered
    attr: the date to get from each movement

    Returns
    -------
    A dictionnary with the dates for each batch

    Relationship to other functions
    ---------------------------
    Called by the following function(s):
        -run_distance_optimization

    """
    return {batch: min([getattr(mov, attr) for
                        mov in d_movements.values() if mov.batch == batch]) for
                        batch in {mov.batch for mov in d_movements.values()}}


def manage_exits(d_movements, d_centres, l_exiting, occupancy):
    """
    Removes individuals from the centres.

    Parameters
    ----------
    d_movements: a dictionnary with every movement considered
    d_centres: a dictionnary with the movements
    l_exiting: a list of exiting batches
    occupancy: the current occupancies of the centers

    Returns
    -------
    A dictionnary with the dates for each batch

    Relationship to other functions
    ---------------------------
    Called by the following function(s):
        -run_distance_optimization

    """
    for exiting in l_exiting:
        occupancy[d_centres[exiting]] -= sum([True for
                                              mov in d_movements.values() if
                                              mov.batch == exiting])


def compute_indirect(df_centre_location, d_movements, l_entering):
    """
    Computes the indirect distance travelled by the bovines, going through each
    centre.

    Parameters
    ----------
    df_centre_location: a table with the latitude and longitude of the centres
    considered
    d_movements: a dictionnary with every movement considered
    l_entering: a list of the entering batches

    Returns
    -------
    A table with the indirect distances computed

    Relationship to other functions
    ---------------------------
    Called by the following function(s):
        -run_distance_optimization

    """
    # create blank output
    df_indirect = pd.DataFrame(columns=df_centre_location.index, dtype='int64')

    for batch in l_entering:
        l_mov = [mov for mov in d_movements.values() if mov.batch == batch]
        d_indirect = {centre.name: sum([mov.indirect_distance(centre) for
                                        mov in l_mov]) for
                      i, centre in df_centre_location.iterrows()}
        df_indirect.at[batch] = pd.Series(d_indirect)

    return df_indirect


def select_batch(df_indirect, l_entering, is_available):
    """
    Selects the batch to assign to a centre based on the total travel distances
    going through the centres.

    Parameters
    ----------
    df_indirect: a table with the total travel distances of each entering batch
    going through each centre.
    l_entering: the list of entering batch
    is_available: a list of boolean indicating whether the centre is available
    (i.e. has not reached its maximal capacity).

    Returns
    -------
    The name of the batch selected

    Relationship to other functions
    ---------------------------
    Called by the following function(s):
        -run_distance_optimization

    """
    # selecting remaining batches and available centres
    df_remaining = df_indirect.loc[l_entering, is_available]

    if sum(is_available) > 1:
        # difference in total travel distance between the best centre and the
        # second best centre
        d_diff = {batch: np.diff(np.sort(total)[:2])[0] for
                  batch, total in df_remaining.iterrows()}
    else:
        # compute total travel distances of the last available centre
        d_diff = {key: val.values for key, val in df_remaining.iterrows()}
    selected = [key for key, val in d_diff.items() if
                val == max(d_diff.values()) and key in l_entering][0]

    return selected


def compute_distance(coord_first, coord_second):
    """
    Computes a distances as the crow flies between two coordinates accroding to
    the spherical law of cosines.

    Parameters
    ----------
    coord_first: the longitude and latitude of the first location
    coord_second: the longitude and latitude of the second location

    Returns
    -------
    The distance computed

    Relationship to other functions
    ---------------------------
    Called by the following function(s):
        -direct_distance
        -indirect_distance
        -origin_to_centre
        -centre_to_destination

    """
    earth_radius = 6371 # in km

    if (coord_first == coord_second).all():
        return 0 # when the origin is identical to the destination

    long_first, lat_first = coord_first*np.math.pi/180 # in radians
    long_second, lat_second = coord_second*np.math.pi/180 # in radians

    return earth_radius * np.math.acos(
        np.math.sin(lat_first) * np.math.sin(lat_second) +
        np.math.cos(lat_first) * np.math.cos(lat_second) *
        np.math.cos(long_first - long_second))

################################ END OF THE CODE ###############################
