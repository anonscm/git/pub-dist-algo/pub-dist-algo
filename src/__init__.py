# -*- coding: utf-8 -*-
"""
Initializes the main codeo of the algorithm of distacnce optimization.

Author: Thibaut Morel-Journel

Written for Python 3.7

"""

from src.algorithm import Movement
from src.algorithm import run_distance_optimization

################################ END OF THE CODE ###############################
