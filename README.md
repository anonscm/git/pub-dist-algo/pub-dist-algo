# Distance optimization algorithm

Author: Thibaut Morel-Journel

Last update: 2020/11/25


## Get the algorithm

The git repository of the algorithm is available on SourceSup. It can be cloned
via SSH using the following command:

`git clone https://git.renater.fr/anonscm/git/pub-dist-algo/pub-dist-algo.git`


## General information

### Requirements

The algorithm is written with Python 3.7.2

The following modules are required:

* Argparse 1.1
* OS
* Pandas 0.24.2
* NumPy 1.16.2
* Sys

One of the additional scripts in written with R.6.0


### Purpose

This repository contains an algorithm aimed at improving the management of 
weaned beef calves during their transport from breeders to fattening units. 
Based on a list of animals, their origin, their destination and the batches they
are assigned to, the algorithm chooses the sorting centres they must go through
in order to minimise their transport distance. 

### Structure of the repository

The folder in which is this README should include the following files:

```
.
|-- README.md
|-- data
|-- scripts
    |-- graphical_output.r
    |-- historical_data.py
|-- src
    |-- __init__.py
    |-- __main__.py
    |-- algorithm.py
|-- test
    |-- data
        |-- centre_capacity.csv
        |-- centre_location.csv
        |-- dataset.csv
        |-- graphical_options.csv
        |-- holding_location.csv
    |-- outcomes
        |-- expected_graphics
            |-- expected_distribution_indirect.png
            |-- expected_distribution_ratio.png
            |-- expected_per_centre.png
        |-- expected_hist_only.csv
        |-- expected_output_raw.csv
```

## Run the algorithm

### Run the optimisation algorithm

To get the help, run the following from this folder:
`python3 -m src -h`

The distance optimisation takes a single argument: the name of the folder
containing the data used to perform the distance optimisation. **Warning**: The 
folder name *test* should not be used except to perform the test of the 
algorithm. The folder must contain the four following files:

* *dataset.csv* : a table with the id of animals in rows, indicating their batch, 
origin, INSEE code of their commune of origin, destination, INSEE code of their 
commune of destination, date of arrival in the sorting centre, date of exit from
the sorting centre.

* *centre_location.csv* : a table with the names of the sorting centres in rows,
indicating their longitude and latitude

* *holding_location.csv* : a table with the INSEE codes of the communes of the
holdings (fattener or breeders) in rows, indicating their longitude and latitude

* *centre_capacity.csv* : a table with the names of sorting centres in rows and 
the years considered in columns, indicating the maximal number of bovines that 
can be present at once in a centre each year.

To perform the optimisation, run the following from this folder: 

`python3 -m src <dataFolderName>` or 

`python -m src <dataFolderName>` for Windows

The output files created will be saved in a folder with the following path 
(automatically created if non-existent): `./outcomes/<dataFolderName>/output_raw.csv`.
This output file is a table with the id of the animals in rows, indicating their
batch, the centre they were assigned to, the direct distance and the indirect
distance they travel.

### Test the algorithm

The test of the algorithm runs in approx. 150 seconds and the output files 
created weight approx. 800 KB. To test whether the algorithm functions properly, 
run the following from this folder: 

`python3 -m src test` or 

`python -m src test` for Windows

This command will run the algorithm and compare the outcomes with the expected
outcomes saved at the following path : `./test/outcomes/expected_output_raw.csv`. 
The algorithm is properly functioning only if none of the outcomes differ. 


## Additional scripts

### Compute the original travel distances

This script can be used on a dataset of historical data, for which the sorting
centres to which the animals were sent is known. Two additional columns are used
from *dataset.csv* :

* *entry_centre* : the first centre in which the bovines arrived. This column is
required.

* *exit_centre* : the last centre from which the batch was sent to the 
destination. If this column is not provided, the exit centre of the batch will
be inferred so that the travel distances of the bovines of the batch is minimal.

To compute the original travel distances, run the following from this folder: 

`python3 scripts/historical_data.py <dataFolderName>` or 

`python scripts/historical_data.py <dataFolderName>` for Windows

The output files created will be saved in a folder with the following path 
(automatically created if non-existent): `./outcomes/<dataFolderName>/`. The
name and content of the output file depends on whether the algorithm was already
run (i.e. the file *output_raw.csv* exists) for this folder:

* *hist_only.csv* if the file *output_raw.csv* is not in the folder. This file
is a table with the id of the animals in rows indicating their batch, centre of
entry, centre of exit and the indirect distance travelled by each animal.

* *output_hist.csv* if the file *output_raw.csv* is not in the folder. This file
combines *output_raw.csv* with the following additional columns: the centre of 
entry, centre of exit and the indirect distance travelled by each animal. 

To test the script, run the following from this folder: 

`python3 scripts/historical_data.py test` or 

`python scripts/historical_data.py test` for Windows

This command will run the algorithm and compare the outcomes with the expected
outcomes saved at the following path : `./test/outcomes/expected_hist_only.csv`. 
The script is properly functioning only if none of the outcomes differ. 

### Plot the default outputs

This script can be used to plot three output graphs based on the output of the
algorithm:

* *distrib_indirect.png* : the distribution of the optimised indirect distances
(and the original ones if available), all centres combined

* *distrib_ratio.png* : the distribution of the ratio between indirect and
direct distances, for the optimised distances and the original if available

* *per_centre* : the raw ratios per centre and the proportion of movements per
centre, for the optimised indirect distances and the original distances if
available

To modify graphical parameters of the script, you can change the file at the
following path: `./data/<dataFolderName>/graphical_options.csv`. Each row must
include the name of the png file to modify ('file'), the argument to change 
('arg') and the new value of the argument ('val').

To plot the default outputs, run the following from this folder:

`Rscript scripts/graphical_output.r <dataFolderName>`

The output files created will be saved in a folder with the following path
(automatically created if non-existent):
`./outcomes/<dataFolderName>/graphics/`.

To test the script, run the following from this folder:
`Rscript scripts/graphical_output.r test`.

This will create the three files in `./outcomes/test/graphics/`, which can be
compared visually with there expected counterparts, located in the following 
folder: `./test/outcomes/expected_graphics/`

<!----------------------------- END OF THE README ----------------------------->
